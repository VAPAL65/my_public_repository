import string

def is_float(str_int):
    if str_int.replace('-','',1).isdigit() and str_int.count('-') < 2:
        return True
    elif str_int.replace('+','',1).isdigit() and str_int.count('+') < 2:
        return True
    else:
        return False


def is_float_with_except(str_int):
    try:
        float(str_int) 
        return True
    except ValueError: 
        return False
    
    
if __name__ == '__main__':
    print(is_float("9835"))
