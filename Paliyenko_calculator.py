#! /usr/bin/python
# -*- coding: utf8 -*-

import argparse

parser = argparse.ArgumentParser()

parser.add_argument("-a",'--arg_a', type=int, default=0,
                    help= "enter first argument a")

parser.add_argument("-b",'--arg_b', type=int, default=0,
                    help= "enter second argument b")

parser.add_argument("-o", '--oper', type=str, default='+',
                    help= "enter operator (Ex. '+', '-', '*', '/')")

args = parser.parse_args()

arg_a = args.arg_a
arg_b = args.arg_b
oper = args.oper

def calc(arg_a, arg_b, oper):
    if oper == '+':
        return arg_a + arg_b
    elif oper == '-':
        return arg_a - arg_b
    elif oper == '*':
        return arg_a * arg_b
    elif oper == '/':
        return arg_a / arg_b
    else:
        return 'Unexepted operation'

if __name__ == '__main__':
    print (f"{arg_a} {oper} {arg_b} = {calc(arg_a, arg_b, oper)}")



    
