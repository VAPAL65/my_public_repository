"""
MainAcademy, Python 3 course, Homework Module_6_1

Create a *.py file that process the following input arguments:

-s source directory
-d destination directoryCopy all *.txt from the source directory and from all nested directories to the destination directory. Print to console information about each copied file in the following format: <datetime>: <source/path/to/file>: <dest/path/to/file>

"""



#! /user/bin/python
# -*- coding: utf8 -*-

import argparse
import shutil 
import sys
import os 
import time
import fnmatch

parser = argparse.ArgumentParser()

parser.add_argument("-s",'--source_directory', type=str, default='source directory',
                    help= "name of source directory")

parser.add_argument("-d", '--destination_directory', type=str, default='destination directory',
                    help= "name of destination directory")

args = parser.parse_args()

source_directory = args.source_directory
destination_directory = args.destination_directory


def check_files(source_directory):
    #all_obj = os.listdir(source_directory)
    all_obj = os.walk(source_directory)
    txt_files = []
    #directories = []
    for root, dirs, files in all_obj:
        #print(fnmatch.fnmatch(obj, '*.txt'))
        for file in files:
            if fnmatch.fnmatch(os.path.join(root, file), '*.txt'):
                txt_files.append(os.path.join(root, file))
        #else:
            #directories.append(obj)
    return txt_files

            
def copy_txt_files(txt_files):
    for file in txt_files:
        shutil.copy(os.path.join(source_directory, file), destination_directory)
        print(f"{time.asctime()}: {os.path.join(source_directory, file)}: {os.path.join(destination_directory, file)}")


if __name__ == '__main__':
        
    files = check_files(source_directory)
    copy_txt_files(files)
    #print(files)
                                                                            
                                                                            
            
# files = os.listdir(source_directory)
# txt_files = files
 #mask = '*.txt'
    
    
